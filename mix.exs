defmodule GVA.MixProject do
  use Mix.Project

  def project do
    [
      app: :global_variable,
      version: "0.1.1",
      elixir: "~> 1.12",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      package: package(),
      description: description(),
      source_url: "https://gitlab.com/locahlost/gva",
      docs: docs()
    ]
  end

  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp deps do
    [
      {:ex_doc, "~> 0.24", only: :dev, runtime: false}
    ]
  end

  defp description do
    "Bring global variable in your Elixir scripts."
  end

  defp package do
    [
      name: "global_variable",
      licenses: ["Do What The Fuck You Want To Public License"],
      links: %{"GitLab" => "https://gitlab.com/locahlost/gva"}
    ]
  end

  defp docs() do
    [
      main: "readme",
      extras: ["README.md"]
    ]
  end
end
