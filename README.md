# Global VAriable

> Tamper perfection with bloat.

## Motivation

- Immutability? Good.
- Mutability? Bad.

When writing a script in Elixir you may need a global scope to save data.
Sure you can `send(self(), {:result, 42})` but it adds annoying overhead to get the sent data.
A `GenServer` can be used but it's worst in term of overhead and absolutely overkill.

GVA is simple and tries to have the smaller footprint when writing scripts: no module or local function to create, etc.
Just an import.

## Installation

In your elixir script:

``` elixir
Mix.install([:global_variable])
import GVA
```

## Notes

- Elixir `1.12` is needed (`then/1` and `Mix.install/1`)
